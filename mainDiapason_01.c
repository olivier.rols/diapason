#include <stdio.h>
#include <stdlib.h>
#include <confuse.h>
#include <sndfile.h>
#include <math.h>
#include "kiss_fft.h"

int main(int argc, char *argv[]) {

    char def_cfg_file_name[] = "diapason.conf";
    char *cfg_file_name;
    FILE *logfile, *outfile;
    SNDFILE *wavfile;
    SF_INFO sfinfo;
    int samplenumber;
    double *samplearray;
    int fftsize;
    kiss_fft_cpx *sampleIn, *fftOut;
    int fftmax;
    double fondfrequ;
    double basepitch,FFTpitch;
    int idx,idy;
    double time,angle;
    double *raw_i, *raw_q;
    double *i, *q;
    int filtersize,PLLfiltersize;
    double temp;
    double deltaf,cents,savarts;
    double gVCO,gF;
    double *sino, *coso, *filterBuffer, *alphaout, *dalphaout;
    int nbzerocross;
    double *period;
    char* notename[] = {"C","C#/Db","D","D#/Eb","E","F","F#/Gb","G","G#/Ab","A","A#/Bb","B"};
    double partitionfreq[12], notefreq, notepartitionfreq,shouldbefreq;
    int octave;

    cfg_opt_t opts[] = {
        CFG_STR("logfile","diapason.log",CFGF_NONE),
        CFG_STR("soundfile", "sound.wav", CFGF_NONE),
        CFG_FLOAT("pitch", 440.0, CFGF_NONE),
        CFG_STR("outputfile","diapason.csv",CFGF_NONE),
        CFG_STR("fftfile","diapasonfft.csv",CFGF_NONE),
        CFG_INT("fftsize", 8192,CFGF_NONE),
        CFG_END()
    };
    cfg_t *cfg;

    cfg = cfg_init(opts, CFGF_NONE);

    /* get config file name */
    if (argc>1) {
        cfg_file_name = argv[1];
    } else {
        cfg_file_name = def_cfg_file_name;
    }

    /* Welcome Print */
    for (idx=0;idx<12;idx++){
        printf("%s, ",notename[idx]);
    }
    printf("hello !\n");

    /* parse config */
    if(cfg_parse(cfg, cfg_file_name) == CFG_PARSE_ERROR) {
        printf("Bad config in %s file\n Bye.\n",cfg_file_name);
        return -1;
    }

    /* open logfile */
    logfile = fopen(cfg_getstr(cfg,"logfile"),"w");
    if (logfile==NULL) {
        printf("Problem openning %s logfile\n Bye.\n",cfg_getstr(cfg,"logfile"));
        return -2;
    }

    /* open file to be analysed */
    sfinfo.format = 0;
    wavfile = sf_open (cfg_getstr(cfg, "soundfile"), SFM_READ, &sfinfo);
    if (wavfile == NULL) {
        fprintf (logfile,"Error : Not able to open input %s wav file.\n", cfg_getstr(cfg, "soundfile"));
        fclose(logfile);
        return -3;
    }

    /* get usefull info */
    fprintf(logfile,"Wav file opened, getting infos :\n");
    fprintf(logfile,"--channels : %d\n",sfinfo.channels);
    fprintf(logfile,"--format : 0x%06x\n",sfinfo.format);
    fprintf(logfile,"--frames : %d\n",(int)sfinfo.frames);
    fprintf(logfile,"--sample rate : %d\n",sfinfo.samplerate);
    fprintf(logfile,"--sections : %d\n",sfinfo.sections);
    fprintf(logfile,"--seekable : %d\n",sfinfo.seekable);

    samplearray = (double*)malloc((size_t) sfinfo.frames * sfinfo.channels * sizeof(*samplearray));
    if (samplearray == NULL) {
        fprintf(logfile,"Memory allocation error for so many samples\n");
        fclose(logfile);
        return -4;
    }
    samplenumber = (int)sf_read_double(wavfile, samplearray,sfinfo.frames);

    fprintf(logfile,"%d samples read \n",samplenumber);
    sf_close(wavfile);

    /* quick FFT to roughly get fundamental frequency */
    fftsize = (int)cfg_getint(cfg,"fftsize");
    kiss_fft_cfg fftcfg = kiss_fft_alloc(fftsize ,0,0,0 );
    sampleIn =(kiss_fft_cpx*)malloc((size_t) fftsize * sizeof(*sampleIn));
    fftOut =(kiss_fft_cpx*)malloc((size_t) fftsize * sizeof(*fftOut));
    if ((sampleIn == NULL)||(fftOut == NULL)) {
        fprintf(logfile,"Memory allocation error for FFT\n");
        fclose(logfile);
        return -4;
    }
    for (idx=0;idx<fftsize;idx++){
        sampleIn[idx].r = (float)samplearray[idx];
        sampleIn[idx].i = 0.0;
    }
    kiss_fft(fftcfg,sampleIn,fftOut);
    fprintf(logfile,"FFT computation on first % d samples \n", fftsize);
    /* save fft file */
    outfile = fopen(cfg_getstr(cfg,"fftfile"),"w");
    if (outfile==NULL) {
        fprintf(logfile,"Problem openning %s file\n Bye.\n",cfg_getstr(cfg,"fftfile"));
        return -2;
    }
    /* writing header */
    fprintf(outfile,"# f R Im \n");
    for (idx=0;idx<fftsize;idx++){
        /* build and write each line */
        fprintf(outfile,"%d %f \n",idx,pow(fftOut[idx].r,2.)+pow(fftOut[idx].i,2.));
    }
    if (idx != fftsize){
        fprintf(logfile,"Problem writing %s file\n Bye.\n",cfg_getstr(cfg,"fftfile"));
        return -2;
    }
    fclose(outfile);
    fprintf(logfile,"%d frequencies writen successfully in %s file\n",fftsize,cfg_getstr(cfg,"fftfile"));

    /* find FFT maximum peak */
    fftmax = 0;
    for (idx=0;idx<fftsize/2;idx++){
        temp = pow(fftOut[idx].r,2.)+pow(fftOut[idx].i,2.);
        if (temp > fftmax) {
            fftmax = temp;
            fondfrequ = idx * (double)sfinfo.samplerate / fftsize;
        }
    }
    fprintf(logfile,"found around %f Hz fondamental frenquency\n",fondfrequ);


    /* rough fondamental frequency is found, let's get some precise measurements */
    FFTpitch = fondfrequ;
    fprintf(logfile,"Analyse %s wavfile at %f Hz\n",cfg_getstr(cfg, "soundfile"),FFTpitch);

    /* demodulation along sample array */
    raw_i = (double*)malloc((size_t) samplenumber * sizeof(*raw_i));
    raw_q = (double*)malloc((size_t) samplenumber * sizeof(*raw_q));
    if ((raw_i == NULL)||(raw_q == NULL)) {
        fprintf(logfile,"Memory allocation error for raw i q 's\n");
        fclose(logfile);
        return -4;
    }
    for (idx=0;idx<(int)sfinfo.frames;idx++) {
        time = (double)idx / (double)sfinfo.samplerate;
        angle = 2*M_PI*FFTpitch*time;
        raw_i[idx] = sin(angle) * samplearray[idx];
        raw_q[idx] = cos(angle) * samplearray[idx];
    }

    /* lowpass 1st order filtering */
    filtersize =(int) sfinfo.samplerate / FFTpitch;
    fprintf(logfile,"1st order low pass filter size is %d samples\n",filtersize);
    i = (double*)malloc((size_t) (samplenumber-filtersize) * sizeof(*raw_i));
    q = (double*)malloc((size_t) (samplenumber-filtersize) * sizeof(*raw_q));
    if ((i == NULL)||(q == NULL)) {
        fprintf(logfile,"Memory allocation error for i q 's\n");
        fclose(logfile);
        return -4;
    }
    /* I Q fast filter and normalisation */
    i[0] = raw_i[0];
    q[0] = raw_q[0];
    for (idy=1;idy<filtersize;idy++) {
        i[0] += raw_i[idy];
        q[0] += raw_q[idy];
    }
    for (idx=1;idx<(int)sfinfo.frames-filtersize;idx++){
        i[idx] = i[idx-1] - raw_i[idx-1] + raw_i[idx+filtersize-1];
        q[idx] = q[idx-1] - raw_q[idx-1] + raw_q[idx+filtersize-1];
    }
    for (idx=0;idx<samplenumber-filtersize;idx++){
        i[idx]/=(double)filtersize;
        q[idx]/=(double)filtersize;
        temp = pow(pow(i[idx],2)+pow(q[idx],2),(1./2.));
        i[idx] /= temp;
        q[idx] /= temp;
    }
    fprintf(logfile,"%d i q terms calculated\n",idx);

    /* try to estimate frequency with phase lock loop */
    sino=(double*)malloc((size_t) (samplenumber-filtersize) * sizeof(*sino));
    coso=(double*)malloc((size_t) (samplenumber-filtersize) * sizeof(*coso));
    filterBuffer=(double*)malloc((size_t) (samplenumber-filtersize) * sizeof(*filterBuffer));
    alphaout=(double*)malloc((size_t) (samplenumber-filtersize) * sizeof(*alphaout));
    dalphaout=(double*)malloc((size_t) (samplenumber-filtersize) * sizeof(*dalphaout));
    if ((sino == NULL)||(coso == NULL)||(filterBuffer == NULL)||(alphaout == NULL)||(dalphaout == NULL)) {
        fprintf(logfile,"Memory allocation error for PLL buffers \n");
        fclose(logfile);
        return -4;
    }
    gVCO = 1/(2* M_PI);
    gF = 1. / 100;
    PLLfiltersize = filtersize;
    coso[0]=0;
    sino[0]=0;
    dalphaout[0]=0;
    alphaout[0]=0;
    filterBuffer[0]=0;
    for (idx=1;idx<(int)sfinfo.frames-filtersize;idx++) {
        /* phase error */
        temp = gF * (q[idx]-coso[idx-1]) / (i[idx]-sino[idx-1]);
        /* filtering */
        if (idx< PLLfiltersize) {
            filterBuffer[idx] = temp;
        } else {
            temp /= PLLfiltersize;
            for (idy=-PLLfiltersize;idy<0;idy++) {
                temp += filterBuffer[idx-idy]/PLLfiltersize;
            }
            filterBuffer[idx] = temp;
        }
        dalphaout[idx] = gVCO * filterBuffer[idx];
        alphaout[idx] = alphaout[idx-1] + dalphaout[idx];
        coso[idx]=cos(alphaout[idx]);
        sino[idx]=sin(alphaout[idx]);
    }

    /* saving output file for gnuplot usage :
    plot [2:4] [-5:5] 'diapason.csv' using 1:2 w l , 'diapason.csv' using 1:3 w l, etc ...
    */
    outfile = fopen(cfg_getstr(cfg,"outputfile"),"w");
    if (outfile==NULL) {
        fprintf(logfile,"Problem openning %s file\n Bye.\n",cfg_getstr(cfg,"outputfile"));
        return -2;
    }
    /* writing header */
    fprintf(outfile,"# x I Q coso sino dalphaout \n");
    for (idx=0;idx<samplenumber-filtersize-1;idx++){
        /* build and write each line */
        time = (double)idx / (double)sfinfo.samplerate;
        fprintf(outfile,"%f %f %f %f %f %f \n",time,i[idx],q[idx],-coso[idx],sino[idx], dalphaout[idx]);
    }
    if (idx != samplenumber-filtersize-1){
        fprintf(logfile,"Problem writing %s file\n Bye.\n",cfg_getstr(cfg,"outputfile"));
        return -2;
    }
    fclose(outfile);
    fprintf(logfile,"%d samples writen successfully in %s file , \n",samplenumber-filtersize-1,cfg_getstr(cfg,"outputfile"));

    /* filter dalphaout along full periods */
    temp = 0.1;
    nbzerocross = 0;
    for (idx=1;idx<samplenumber-filtersize-1;idx++){
        /* test zero crossing */
        if (temp * sino[idx] < 0.0) {
            nbzerocross++;
            temp = sino[idx];
        }
    }
    fprintf(logfile,"%d zero crossing detected \n",nbzerocross);
    period =(double*)malloc((size_t) (nbzerocross + 1) * sizeof(*period));
    if (period == NULL) {
        fprintf(logfile,"Memory allocation error for periods measurement \n");
        fclose(logfile);
        return -4;
    }
    temp = 0.1;
    nbzerocross = 0;
    period[0] = 0.;
    for (idx=1;idx<samplenumber-filtersize-1;idx++){
        /* test zero crossing */
        if (temp * sino[idx] < 0.0) {
            nbzerocross++;
            temp = sino[idx];
            period[nbzerocross] = (double)idx / (double)sfinfo.samplerate;
        }
    }
    for (idx=nbzerocross;idx>1;idx--) {
        period[idx] -= period[idx-2];
    }
    if (nbzerocross<4) {
        fprintf(logfile,"Not enough sample time to analyse \n");
        fclose(logfile);
        return -5;
    }
    /* enough periods detected, let's continue ... */

    /* initial "partition" generation for equal temperament virtual keyboard
    add other temperaments for future : Pythagore, Zarlino, Cordier */
    basepitch = cfg_getfloat(cfg, "pitch");
    for (idx=0;idx<12;idx++){
        partitionfreq[idx] = basepitch * exp(((double)idx-9.)/12.*log(2.));
    }
    fprintf(logfile,"Equal temperament tuner choosen @ %.1f Hz diapason\n",basepitch);

    for (idx=4;idx<nbzerocross;idx++){
        fprintf(logfile,"--- Period %d Estimation ---\n",idx);
        /* fprintf(logfile,"length is %f s \n",period[idx]);*/
        deltaf = 1 / period[idx];
        notefreq = FFTpitch+deltaf;
        fprintf(logfile,"fondamental frequency is %f Hz, ",notefreq);
        /* frequency to notes conversion */
        /* octave computation : 4th for A 440, add 1/24 to round to higher octave between B and C */
        octave = (int)(4. + log2(notefreq/partitionfreq[0]) + 1./24.);
        /* frequency transposed to medium octave a.k.a. "partition" */
        notepartitionfreq = pow(2.,(4.0-(double)octave)) * notefreq;
        /* nearest note */
        idy = (int) (log2(notepartitionfreq/basepitch)*12. + 9. +0.5 );
        /* Verify that idy is in {0...11} */
        if (idy<0 || idy>11) {
            fclose(logfile);
            printf("comput error\n");
            return -6;
        }
        shouldbefreq = pow(2.,(double)octave - 4.0) * partitionfreq[idy];
        fprintf(logfile," note is (%s)%d",notename[idy],octave);
        fprintf(logfile," its frequency should be %f Hz\n",shouldbefreq);
        deltaf = notefreq - shouldbefreq;
        fprintf(logfile,"frequency difference = %f Hz",deltaf);
        /* traduction in usual units */
        cents = 1200.0 * log2(notefreq/shouldbefreq);
        savarts = 1000.0 * log10(notefreq/shouldbefreq);
        fprintf(logfile,", or %2.1f cents ",fabs(cents));
        fprintf(logfile,"/ %2.1f savarts",fabs(savarts));
        if (deltaf > 0) fprintf(logfile," too high.\n"); else fprintf(logfile," too low.\n");
    }

    /* end job */
    free(period);
    free(sino);
    free(coso);
    free(filterBuffer);
    free(alphaout);
    free(dalphaout);
    free(i);
    free(q);
    free(samplearray);
    free(raw_i);
    free(raw_q);
    free(sampleIn);
    free(fftOut);
    kiss_fft_free(fftcfg);
    fclose(logfile);
    cfg_free(cfg);
    return 0;
}
