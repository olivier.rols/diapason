# Diapason

(Yet another) software tuner. This one's goal is precision measurement...

General principe to measure frequency (f) is inspired by strobo tuners.
It uses demodulation and measures frequency difference.
Nearest octave, note and "perfect" frequency (fP) are found directly by math. 
Compute tuning error f relatively to fP in calculated usual music units :
semitone cents and savart.

See wiki page for more details.